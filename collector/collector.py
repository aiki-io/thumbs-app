
from twython import TwythonStreamer


class TweetStreamer(TwythonStreamer):
    def on_success(self, data):
        if 'text' in data:
            print( data['text'].encode('utf-8'))

    def on_error(self, status_code, data):
        print(status_code)
        self.disconnect()


if __name__=='__main__':
